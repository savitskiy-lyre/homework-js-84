const User = require('../models/User');

const authUser = async (req, res, next) => {
   const {username, password} = req.body;
   try {
      const user = await User.findOne({username});
      if (!user) return res.status(401).send({error: 'Username not found'});
      const isMatch = await user.checkPassword(password);
      if (!isMatch) return res.status(401).send({error: 'Password is wrong'});
      req.user = user;
   } catch (err) {
      res.sendStatus(500);
   }
   next();
}

module.exports = authUser;