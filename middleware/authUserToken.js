const User = require('../models/User');

const authUserToken = async (req, res, next) => {
   const reqToken = req.get('Authorization');
   if (!reqToken) return res.status(401).send({error: 'Please log in first'});
   try {
      const user = await User.findOne({token: reqToken});
      if (!user) return res.status(401).send({error: 'Wrong authorization token'});
      req.user = user;
   } catch (err) {
      res.sendStatus(500);
   }
   next();
}

module.exports = authUserToken;