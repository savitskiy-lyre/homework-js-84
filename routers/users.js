const express = require('express');
const router = express.Router();
const authUser = require('../middleware/authUser');
const User = require('../models/User');

router.get('/', async (req, res) => {
   try {
      const users = await User.find();
      res.send(users);
   } catch (e) {
      res.sendStatus(500)
   }
});
router.post('/', async (req, res) => {

   const {username, password} = req.body;
   if (!username || !password) return res.sendStatus(400);
   const user = new User({username, password});
   try {
      user.generateToken();
      await user.save();
      res.send(user);
   } catch (err) {
      res.sendStatus(500);
   }
});

router.post('/sessions', authUser, async (req, res) => {
   try {
      req.user.generateToken();
      await req.user.save();
      res.send({token: req.user.token});
   } catch (err) {
      res.sendStatus(500)
   }

});

module.exports = router;