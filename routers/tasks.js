const express = require('express');
const router = express.Router();
const Task = require('../models/Task');
const authUserToken = require('../middleware/authUserToken');
const giveCheckedEssence = require('../utilities/giveCheckedEssence');

router.get('/', authUserToken, async (req, res) => {
   try {
      const tasks = await Task.find({user: req.user._id});
      res.send(tasks);
   } catch (err) {
      res.sendStatus(500);
   }
});

router.post('/', authUserToken, async (req, res) => {
   const {title, description, status} = req.body;
   if (!title || !status) return res.status(400).send({error: 'Data not valid'});
   try {
      const task = await new Task(giveCheckedEssence({title, description, status, user: req.user._id}));
      await task.save();
      res.send(task);
   } catch (err) {
      res.status(500).send({error: err.message});
   }
});
router.put('/:id', authUserToken, async (req, res) => {
   const {title, description, status} = req.body;
   const {id: reqId} = req.params;
   if (!reqId) return res.status(400).send({error: 'Please enter task id'});
   try {
      const task = await Task.findOne({_id: reqId, user: req.user._id});
      if (!task) return res.status(404).send({error: 'Not found'});
      const updateForTask = giveCheckedEssence({title, description, status});
      for (let key in updateForTask) {
         if (updateForTask.hasOwnProperty(key)) {
            task[key] = updateForTask[key];
         }
      }
      await task.save();
      res.send(task);
   } catch (err) {
      res.status(500).send({error: err.message});
   }
});
router.delete('/:id', authUserToken, async (req, res) => {
   const {id: reqId} = req.params;
   if (!reqId) return res.status(400).send({error: 'Please enter task id'});
   try {
      const task = await Task.findOneAndDelete({_id: reqId, user: req.user._id});
      if (!task) return res.status(404).send({error: 'Not found'});
      res.send({message: 'Deletion completed'});
   } catch (err) {
      res.status(500).send({error: err.message});
   }
});

module.exports = router;