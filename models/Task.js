const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TaskSchema = new mongoose.Schema({
   user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'users',
   },
   title: {
      type: String,
      required: true,
   },
   description: String,
   status: {
      type: String,
      required: true,
      enum: ["new","in progress","completed"],
   }
})

TaskSchema.plugin(idValidator);
const Task = mongoose.model('tasks', TaskSchema);
module.exports = Task;