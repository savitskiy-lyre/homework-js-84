const express = require('express');
const exitHook = require('async-exit-hook');
const mongoose = require('mongoose');
const users = require('./routers/users');
const tasks = require('./routers/tasks');

const cors = require('cors');

const server = express();
const port = 7000;

server.use(express.json());
server.use(cors());

server.get('/', (req,res) => {
   res.send("working");
})

server.use('/users', users);
server.use('/tasks', tasks);

const run = async () => {
   await mongoose.connect('mongodb://localhost/toDoStorage');

   server.listen(port, () => {
      console.log(`Server is started on ${port} port !`);
   })

   exitHook(() => {
      console.log('exiting');
      mongoose.disconnect();
   });
}

run().catch(e => console.error(e));